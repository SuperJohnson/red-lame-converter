#!/bin/bash

# Prerequisites :
## - Please install lame to make this script work :
## sudo apt-get install lame
## - Please make sure you have writing rights on all your target folders and subfolders.
## If not, you could use sudo to make this script work anyway.

# What this script does :
## Transcode flac, alac and wav files into mp3 recursively, using lame best possible parameters.

# How to use :
## - Place all your audio files to transcode in the same folder as this script folder. It can handle as many subfolders as you want, don't worry about it.
## - Execute the script using bash : bash RedLameConverter/RedLameConverter.sh [mode] [replaygain] [delsource]
## - Possible values for mode : v0 (default) | 320 => uses VBR best quality params or CBR best quality params.
## - Possible values for replaygain : accurate (default) | fast | nogain => compute ReplayGain accurately and find the peak sample, fast but slightly inaccurately, or disable ReplayGain analysis.
## - Possible values for delsource : nodel (default) | del => doesn't delete source files or delete it after the transcode is done.
## - Usecase exemple : bash RedLameConverter/RedLameConverter.sh v0 accurate del

# More about lame
## - Lame manual : https://linux.die.net/man/1/lame
## - Lame licence informations : http://lame.sourceforge.net/license.txt

# Licence
## MIT : https://opensource.org/licenses/MIT

# Default arguments values
if [ -z "$1" ]
then
	mode="v0"
elif [ "$1" = "v0" ] || [ "$1" = "320" ]
then
	mode="$1"
else
	mode="v0"
fi
if [ -z "$2" ]
then
	replaygain="--replaygain-accurate"
elif [ "$2" = "accurate" ]
then
	replaygain="--replaygain-accurate"
elif [ "$2" = "fast" ]
then
	replaygain="--replaygain-fast"
elif [ "$2" = "nogain" ]
then
	replaygain="--noreplaygain"
else
	replaygain="--replaygain-accurate"
fi
if [ -z "$3" ]
then
	delsource="nodel"
elif [ "$3" = "del" ] || [ "$3" = "nodel" ]
then
	delsource="$3"
else
	delsource="nodel"
fi

# Source extensions table
exts=("flac" "alac" "wav" "aac" "m4a")

# For each source file extension
for ext in ${exts[@]}
do
	# One by one process
	find $PWD -name "*."${ext} | while read file; do
		outputfile="${file/${ext}/'mp3'}"
		# v0 transcode
		if [ $mode = "v0" ]
		then
			lame -q0 -V0 --vbr-new "$replaygain" "$file" "$outputfile"
		# 320 transcode
		else
			lame --cbr -b 320 -q0 "$replaygain" "$file" "$outputfile"
		fi
		# Delete source files if specified
		if [ $delsource = "del" ]
		then
			rm "$file"
		fi
	done
done
echo "Transcode ended!"
