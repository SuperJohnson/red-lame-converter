# RedLameConverter

Transcode flac, alac and wav files into mp3 recursively, using Lame best possible parameters


# Purpose

There is always something wrong with MP3 transcoders on Linux. I made a really tiny CLI bash script that is slower to transcode because it doesn't handle many cores at the same time, but at list it uses the best Lame parameters, my opinion.


# What it does exactly

It allows you to transcode flac, alac or wav files to mp3 using lame.
You can choose CBR320 or VBRv0 as an output, define a replaygain behavior, and delete source files after the conversion if you want.
The script handle as many music subfolders as you want.


# Prerequisites

- You need to install Lame to make this script work :
```sh sudo apt-get install lame```


# How to use it

- Install git :
```sh
sudo apt-get install git
```
- Copy the music folders wherever you want, for exemple ~/Transcode
- Move into the folder you choosed then download the script :
```sh
cd ~/Transcode
git clone https://github.com/SuperJohnson0/RedLameConverter.git
```
- Execute the script using bash :
```sh bash RedLameConverter/RedLameConverter.sh [mode] [replaygain] [delsource]```                


* Possible values for mode :
    * __v0 (default)__ → uses VBR best quality params
    * __320__ → uses CBR best quality params
* Possible values for replaygain :
    * __accurate (default)__ → applies --replaygain-accurate, compute ReplayGain accurately and find the peak sample
    * __fast__ → applies --replaygain-fast, faster but slightly inaccurately
    * __nogain__ → applies --noreplaygain, disable ReplayGain analysis
* Possible values for delsource :
    * __nodel (default)__ → doesn't delete source files after the transcode is done
    * __del__ → deletes source files after the transcode is done
* Usecase exemple :
```sh bash RedLameConverter/RedLameConverter.sh v0 accurate del```


# More about Lame

- Lame manual : https://linux.die.net/man/1/lame
- Lame licence informations : http://lame.sourceforge.net/license.txt


# Licence
MIT : https://opensource.org/licenses/MIT
